FROM php:7.4-fpm

USER root

# Copy composer.lock and composer.json into the working directory
COPY composer.lock composer.json /var/www/

# Set working directory
WORKDIR /var/www/

ARG DEBIAN_FRONTEND=noninteractive

# Install dependencies
RUN apt-get update && apt-get install -y apt-utils
RUN	apt-get install -y build-essential \
	libfreetype6-dev \ 
	libjpeg62-turbo-dev \ 
	libpng-dev \
	libwebp-dev \ 
    libzip-dev \
	libonig-dev \
	locales \
	zip \
	gcc g++ exif \
    jpegoptim optipng pngquant gifsicle \
	vim \
	unzip \
	git \
	curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*


# Install extensions
RUN docker-php-ext-install pdo pdo_mysql mbstring zip exif pcntl opcache
RUN docker-php-ext-configure gd
RUN docker-php-ext-install gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Copy files
COPY . /var/www

COPY --chown=root:root . /var/www
# Assign permissions of the working directory to the www-data user
RUN chown -R root:root /var/www
RUN chmod -R +x /var/www/storage
RUN chmod -R +x /var/www/bootstrap/cache

EXPOSE 80 443
#CMD ["php-fpm"]
#RUN ["chmod", "+x", "/var/www/post_deploy.sh"]

#CMD [ "sh", "./post_deploy.sh" ]
CMD ["php-fpm", "php artisan serve --port=80"]
